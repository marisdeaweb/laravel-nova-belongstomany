<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')
          ->insert([
              'name' => 'Gulliver\'s Travels',
              'created_at' => now(),
              'updated_at' => now(),
          ]);

        DB::table('book_user')
          ->insert([
              'book_id' => 1,
              'user_id' => 1,
              'bought_on' => '2019-05-12',
              'created_at' => now(),
              'updated_at' => now(),
          ]);
    }
}
