<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BookUser extends Pivot
{
    /**
     * @var array
     */
    public $dates = ['bought_on'];
}
